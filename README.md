# Finding Prime Numbers algorithms
> Project include three diffrent algorithms to find prime numbers

## Table of Contents
* [General Info](#general-information)
* [Technologies Used](#technologies-used)
* [Features](#features)
* [Project Status](#project-status)
* [Contact](#contact)


## General Information
- This project was realized on IV semester of studies.
- Program include three diffrent algorithms to find prime numbers
- The purpose of project was to learn diffrents in similar algorithms


## Technologies Used
- Python - version 3.11
- PyCharm Community Edition


## Features
Provided features:
- Three diffrent way to find if numer is prime
- Comprasion time and accuracy in finding those numbers
- Description with results of those algorithms



## Project Status
Project is: _no longer being worked on_. 


## Contact
Created by Piotr Hajduk

