import numpy as np
import random
import time


def potega_modulo_bin(a, b, n):
    result = 0
    temp = 1
    b = bin(b)[2:]
    for i in range(len(b)):
        if_good = (int(b[len(b) - 1 - i]))
        if i == 0:
            res = a % n
        else:
            res = (a * a) % n
        a = res
        if if_good == 1:
            result = (temp * res) % n
            temp = result
    return result


def pierwsza_1(a):
    p = 1
    n = int(np.sqrt(a))
    danger_num_g = [2, 3, 5, 7]
    danger_num_b = [1, 4, 6, 8, 9]
    if a in danger_num_g:
        return 1
    if a in danger_num_b:
        return 0

    for i in range(2, n + 1):
        if a % i == 0:
            p = 0
            return p
    return p


def pierwszaF(a, s):
    p = 1
    danger_num_g = [2, 3]
    danger_num_b = [1]
    if a in danger_num_g:
        return 1
    if a in danger_num_b:
        return 0
    for i in range(1, s + 1):
        t = random.randint(2, a - 2)
        if potega_modulo_bin(t, a - 1, a) != 1:
            p = 0
            return p
    return p


def pierwszaMR(a, s):
    p = 1
    danger_num_g = [2,3]
    danger_num_b = [1]
    if a in danger_num_g:
        return 1
    if a in danger_num_b:
        return 0
    if a%2 ==0:
        return 0
    u, r = ur(a)
    for i in range(1, s + 1):
        t = random.randint(2, a - 2)
        z = potega_modulo_bin(t, r, a)
        if z != 1:
            j = 0
            while z != a - 1:
                z = potega_modulo_bin(z, 2, a)
                j += 1
                if z==1 or j==u:
                    p = 0
                    return p
    return p

def ur(a):
    u = 0
    r = a - 1
    while r % 2 == 0:
        r = r // 2
        u += 1
    return u, r


def pierwszaMR2(a, s):
    p = 1
    danger_num_g = [2,3]
    danger_num_b = [1]
    if a in danger_num_g:
        return 1
    if a in danger_num_b:
        return 0
    if a%2 ==0:
        return 0
    u, r = ur(a)
    tab = []
    for i in range(2, a - 1):
        tab.append(i)
    for i in range(1, s + 1):
        t = np.random.choice(tab)
        tab.remove(t)
        z = potega_modulo_bin(t, r, a)
        if z != 1:
            j = 0
            while z != a - 1:
                z = potega_modulo_bin(z, 2, a)
                j += 1
                if z==1 or j==u:
                    p = 0
                    return p
    return p


# Tworzenie zestawow do testów

same_pierwsze = []
same_zlozone = []
wszystkie_liczby = []
for i in range(10,10**6+1):
    wszystkie_liczby.append(i)
    if pierwsza_1(i):
        same_pierwsze.append(i)
    else:
        same_zlozone.append(i)

licznik_test_1 = 0
t = time.time()
for i in wszystkie_liczby:
    if pierwsza_1(i):
        licznik_test_1 += 1
tl_1 = time.time() - t
print(tl_1)
filename = 'output_1.txt'
file = open(filename, 'w')
file.write(f'Liczba liczb pierwszych w zakresie od 10 do 1 000 000: {licznik_test_1}\n'
           f'Czas potrzebny do znalezienia liczb pierwszych: t = {tl_1}\n')
file.close()


czasy = []
testy = []
liczby_pierwsze = []
ilosc_zlych_liczb = []
for s in range(0,51,5):
    print(s)
    licznik_test_2 = 0
    t = time.time()
    for i in wszystkie_liczby:
        if pierwszaF(i,s):
            licznik_test_2 += 1
    tl_2 = time.time() - t
    bledne_liczby = licznik_test_2 - licznik_test_1
    czasy.append(tl_2)
    testy.append(s)
    liczby_pierwsze.append(licznik_test_2)
    ilosc_zlych_liczb.append(bledne_liczby)

filename = 'output_2.txt'
file = open(filename, 'w')
file.write(f'Liczba liczb pierwszych w zakresie od 10 do 1 000 000: {liczby_pierwsze}\n'
           f'Ilosc testów w iteracji {testy}\n'
           f'Czas potrzebny do znalezienia liczb pierwszych: t = {czasy}\n'
           f'Ilosc liczb zlozonych uznanych za pierwsze {ilosc_zlych_liczb}\n')
file.close()


czasy = []
testy = []
liczby_pierwsze = []
ilosc_zlych_liczb = []
for s in range(0,51,5):
    licznik_test_2 = 0
    t = time.time()
    for i in wszystkie_liczby:
        if pierwszaMR(i,s):
            licznik_test_2 += 1
    tl_2 = time.time() - t
    bledne_liczby = licznik_test_2 - licznik_test_1
    czasy.append(tl_2)
    testy.append(s)
    liczby_pierwsze.append(licznik_test_2)
    ilosc_zlych_liczb.append(bledne_liczby)

filename = 'output_3.txt'
file = open(filename, 'w')
file.write(f'Liczba liczb pierwszych w zakresie od 10 do 1 000 000: {liczby_pierwsze}\n'
           f'Ilosc testów w iteracji {testy}\n'
           f'Czas potrzebny do znalezienia liczb pierwszych: t = {czasy}\n'
           f'Ilosc liczb zlozonych uznanych za pierwsze {ilosc_zlych_liczb}\n')
file.close()


